const path = require('path')

module.exports = {
  resolve: {
    extensions: ['.js', '.json', '.ts'],
    alias: {
      '@': path.resolve(__dirname + "/src")
    },
  }
}
