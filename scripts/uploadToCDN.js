var qiniu = require("qiniu");
var fs = require("fs");

require('dotenv').config();

var accessKey = process.env.accessKey;
var secretKey = process.env.secretKey;
var bucket = process.env.bucket;
var website_name = process.env.websiteName;

var mac = new qiniu.auth.digest.Mac(accessKey, secretKey);

var options = {
  scope: bucket
};

var putPolicy = new qiniu.rs.PutPolicy(options);

var uploadToken = putPolicy.uploadToken(mac);
var config = new qiniu.conf.Config();
config.zone = qiniu.zone.Zone_z2;

var formUploader = new qiniu.form_up.FormUploader(config);
var bucketManager = new qiniu.rs.BucketManager(mac, config);
var cdnManager = new qiniu.cdn.CdnManager(mac);


function deleteExistFiles(fileNames) {

  var deleteOperations = fileNames.map(function (name) {
    return qiniu.rs.deleteOp(bucket, name)
  });

  return new Promise(function (resolve, reject) {
    bucketManager.batch(deleteOperations, function(err, respBody, respInfo) {
      if (err) {
        console.error("delete all files failed")
        reject(err);
        //throw err;
      } else {
        console.log("delete all files done")
        resolve()
      }
    });
  })
}

function uploadSingleFile(filename, localPath) {
  var putExtra = new qiniu.form_up.PutExtra();

  putExtra.mimeType = filename.indexOf("css") !== -1 ? 'text/css' : 'application/javascript';

  return new Promise(function (resolve, reject) {
    formUploader.putFile(uploadToken, filename, localPath, putExtra, function(respErr, respBody, respInfo) {
      if (respErr) {
        throw respErr;
      }
      if (respInfo.statusCode === 200) {
        resolve(respBody)
      } else {
        reject(respBody)
      }
    });
  })
}

function refreshCDNFiles(fileNames) {
  let urlsToRefresh = fileNames.map(function (filename) {
    return website_name + filename
  });

  return new Promise(function (resolve, reject) {
    cdnManager.refreshUrls(urlsToRefresh, function(err, respBody, respInfo) {
      if (err) {
        reject(err)
      }
      console.log(respInfo.statusCode);
      if (respInfo.statusCode === 200) {
        resolve()
      }
    });
  })
}

function getFileList() {
  return new Promise(function (resolve, reject) {
    fs.readdir('./dist', (err, files) => {
      if (!err) {
        resolve(files)
      } else {
        reject(err)
      }
    })
  })
}

async function main() {
  let fileList = await getFileList();

  await deleteExistFiles(fileList);

  await Promise.all(fileList.map(function (filename) {
    return uploadSingleFile(filename, './dist/' + filename)
  }));

  console.log(`${fileList.length} has been uploaded` );

  //await refreshCDNFiles(fileList);
  //console.log(`${fileList.length} has been refreshed` )
}

main();
