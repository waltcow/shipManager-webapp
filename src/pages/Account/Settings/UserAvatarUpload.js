import React, { Component, Fragment } from 'react';
import { formatMessage, FormattedMessage } from 'umi/locale';
import { Button, Upload, message } from 'antd'
import styles from './BaseView.less';
import OSSClient from '@/utils/OSSClient';

class UserAvatarUpload extends Component {

  state = {
    loading: false,
    preview: "",
    visible: false,
    updateURL: "",
    imageList: [],
  };

  beforeUpload(file) {
    const isLt2M = file.size / 1024 / 1024 < 2;

    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
      return false
    }

    let reader = new FileReader();
    let client = OSSClient.getInstance();
    let self = this;

    reader.readAsDataURL(file);

    this.setState({ loading: true });

    reader.onloadend = () => {
      client.updateToOss(file).then(data => {
        let url = data.res && data.res.requestUrls[0] || "";
        self.setState({ loading: false, updateURL: url });
        self.props.onUpdate(url);
      }).catch(err => {
        self.setState({ loading: false });
        console.error(err)
      })
    };

    return false
  }

  componentDidMount() {
    OSSClient.getInstance()
  }

  render() {
    const { avatar } = this.props;

    const props = {
      beforeUpload: this.beforeUpload.bind(this),
      fileList: this.state.imageList,
      accept: "image/*",
    };

    return (
      <Fragment>
        <div className={styles.avatar_title}>个人头像</div>
        <div className={styles.avatar}>
          <img src={this.state.updateURL === "" ? avatar : this.state.updateURL } alt="avatar" />
        </div>
        <Upload {...props}>
          <div className={styles.button_view}>
            <Button icon="upload" loading={this.state.loading}>
              <FormattedMessage id="app.settings.basic.avatar" defaultMessage="Change avatar" />
            </Button>
          </div>
        </Upload>
    </Fragment>
    )
  }
}

export default UserAvatarUpload;
