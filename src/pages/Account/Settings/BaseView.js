import React, { Component, Fragment } from 'react';
import { formatMessage, FormattedMessage } from 'umi/locale';
import { Form, Input, Select, Button, message } from 'antd';
import { connect } from 'dva';
import styles from './BaseView.less';
import UserAvatarUpload from './UserAvatarUpload';

const FormItem = Form.Item;
const { Option } = Select;

@connect(({ user, loading }) => ({
  currentUser: user.currentUser,
  submitting: loading.effects['user/update']
}))
@Form.create()
class BaseView extends Component {

  state = {
    updateAvatarURL: ""
  }

  componentDidMount() {
    const { currentUser, dispatch } = this.props;

    if (!currentUser.id) {
      dispatch({
        type: 'user/fetchCurrent',
        callback: () => this.setBaseInfo()
      });
      return;
    }

    this.setBaseInfo();
  }

  setBaseInfo = () => {
    const { currentUser, form } = this.props;
    Object.keys(form.getFieldsValue()).forEach(key => {
      const obj = {};
      obj[key] = currentUser[key] || null;
      form.setFieldsValue(obj);
    });
  };

  getAvatarURL() {
    const { currentUser } = this.props;
    if (currentUser.avatar) {
      return currentUser.avatar;
    }
    return 'https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png';
  }

  handleAvatarUpdate = (url) => {
    this.setState({ updateAvatarURL: url });
    message.success("新头像上传成功")
  }

  handleSubmit = e => {
    const { dispatch, form } = this.props;
    e.preventDefault();

    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        if (this.state.updateAvatarURL !== "") {
          values.avatar = this.state.updateAvatarURL;
        }
        dispatch({
          type: 'user/update',
          payload: values,
          callback: () => {
            message.success("信息更新成功")
          }
        });
      }
    });
  };

  getViewDom = ref => {
    this.view = ref;
  };

  render() {
    const {
      form: { getFieldDecorator },
      submitting
    } = this.props;
    return (
      <div className={styles.baseView} ref={this.getViewDom}>
        <div className={styles.left}>
          <Form layout="vertical" onSubmit={this.handleSubmit} hideRequiredMark>
            <FormItem label={formatMessage({ id: 'app.settings.basic.email' })}>
              {getFieldDecorator('email', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.settings.basic.email-message' }, {}),
                  },
                ],
              })(<Input />)}
            </FormItem>
            <FormItem label={formatMessage({ id: 'app.settings.basic.nickname' })}>
              {getFieldDecorator('userName', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.settings.basic.nickname-message' }, {}),
                  },
                ],
              })(<Input />)}
            </FormItem>
            <FormItem label={formatMessage({ id: 'app.settings.basic.profile' })}>
              {getFieldDecorator('description', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.settings.basic.profile-message' }, {}),
                  },
                ],
              })(
                <Input.TextArea
                  placeholder={formatMessage({ id: 'app.settings.basic.profile-placeholder' })}
                  rows={4}
                />
              )}
            </FormItem>
            <FormItem label={formatMessage({ id: 'app.settings.basic.phone' })}>
              {getFieldDecorator('phone', {
                rules: [
                  {
                    required: true,
                    message: formatMessage({ id: 'app.settings.basic.phone-message' }, {}),
                  }
                ],
              })(<Input style={{ width: '100%' }} />)}
            </FormItem>
            <Button type="primary" loading={submitting} onClick={this.handleSubmit}>
              <FormattedMessage
                id="app.settings.basic.update"
                defaultMessage="Update Information"
              />
            </Button>
          </Form>
        </div>
        <div className={styles.right}>
          <UserAvatarUpload avatar={this.getAvatarURL()} onUpdate={this.handleAvatarUpdate} />
        </div>
      </div>
    );
  }
}


export default BaseView;
