import React, { PureComponent } from 'react';
import {
  Card,
  Button,
  Form,
  Input,
  Radio,

} from 'antd';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import {message} from "antd/lib/index";
import {routerRedux} from "dva/router";

const FormItem = Form.Item;
const { TextArea } = Input;

@connect(({ sailor, loading }) => ({
  sailor: sailor.sailor,
  submitting: loading.effects['sailor/update'],
}))
@Form.create()
class UpdatedSailorForm extends PureComponent {

  handleSailorUpdated = () => {
    message.success('船员信息已更新');
    this.props.dispatch(routerRedux.push('/sailor/list'));
  };

  setSailorInfo = () => {
    const { sailor, form } = this.props;

    Object.keys(form.getFieldsValue()).forEach(key => {
      const obj = {};
      obj[key] = sailor[key] || null;
      form.setFieldsValue(obj);
    });
  };

  componentWillMount() {
    if (this.props.match.params.id) {
      let sailorId = parseInt(this.props.match.params.id, 10);
      setTimeout(() => {
        this.props.dispatch({
          type: "sailor/load",
          payload: sailorId,
          callback: this.setSailorInfo
        })
      }, 10)
    }
  }

  handleSubmit = (e) => {
    const { dispatch, form, sailor } = this.props;
    e.preventDefault();
    form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        dispatch({
          type: 'sailor/update',
          payload: { id: sailor.id, sailor: values},
          callback: this.handleSailorUpdated
        });
      }
    });
  };

  render() {

    const {
      submitting,
      form: { getFieldDecorator }
    } = this.props;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 12 },
        md: { span: 10 },
      },
    };

    const submitFormLayout = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 10, offset: 4 },
      },
    };

    return (
      <PageHeaderWrapper
        title="更新船员信息"
        content="按表单提示填入相应信息"
      >
        <Card title="基本信息" bordered={false}>
          <Form onSubmit={this.handleSubmit} hideRequiredMark style={{ marginTop: 8 }}>
            <FormItem {...formItemLayout} label="船员姓名">
              {getFieldDecorator('name', {
                rules: [
                  {
                    required: true,
                    message: '请输入姓名',
                  },
                ],
              })(<Input placeholder="请输入姓名" />)}
            </FormItem>

            <FormItem {...formItemLayout} label="常任职位">
              {getFieldDecorator('position', {
                rules: [
                  {
                    required: true,
                    message: '请输入职位',
                  },
                ],
              })(<Input placeholder="请输入职位" />)}
            </FormItem>

            <FormItem {...formItemLayout} label="手机号码">
              {getFieldDecorator('mobile', {
                rules: [
                  {
                    required: true,
                    message: '请输入手机',
                  },
                ],
              })(<Input placeholder="请输入手机" />)}
            </FormItem>

            <FormItem {...formItemLayout} label="家庭地址">
              {getFieldDecorator('address', {
                rules: [
                  {
                    required: true,
                    message: '请输入联系地址',
                  },
                ],
              })(<TextArea style={{ minHeight: 32 }} placeholder="请输入联系地址" rows={4} />)}
            </FormItem>

            <FormItem {...formItemLayout} label="是否为高级船员">
              {getFieldDecorator('isAdvanced', {
                initialValue: 0,
              })(
                <Radio.Group>
                  <Radio value={true}>是</Radio>
                  <Radio value={false}>不是</Radio>
                </Radio.Group>
              )}
            </FormItem>

            <FormItem {...submitFormLayout} style={{ marginTop: 32 }}>
              <Button type="primary" htmlType="submit" loading={submitting}>
                保存
              </Button>
            </FormItem>
          </Form>
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default UpdatedSailorForm;
