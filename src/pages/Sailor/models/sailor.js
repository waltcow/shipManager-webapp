import { querySailorList, createSailor, deleteSailor, updateSailor } from '@/services/SailorAPI';
import { routerRedux } from 'dva/router';
import { find } from 'lodash'

export default {
  namespace: 'sailor',

  state: {
    sailor: {},
    data: {
      list: [],
      pagination: {},
    },
  },

  effects: {

    *fetch({ payload }, { call, put }) {
      let result = yield call(querySailorList, payload);
      if (result === undefined) {
        return false
      }
      const { code, data } = result;
      if (code === "S_OK" && data.list.length > 0) {

        data.list = data.list.map(item => {
          item.key = item.id;
          return item;
        });
        data.pagination.current = data.pagination.current + 1;

        yield put({
          type: 'save',
          payload: data,
        });
      }
    },

    *load({ payload, callback }, { put, select }) {

      // todo: fetch sailor when list is empty
      let sailors = yield select(state => state.sailor.data.list);

      if (sailors.length > 0) {
        let sailor = find(sailors, (item) => item.key === payload);

        yield put({
          type: 'reload',
          payload: sailor,
        });
        callback && callback()
      }
    },

    *create({ payload, callback }, { call }) {
      let result = yield call(createSailor, payload);
      callback && callback()
    },

    *remove({ payload, callback }, { call, put }) {

      let result = yield call(deleteSailor, payload);

      yield put({
        type: 'delete',
        payload: payload,
      });

      callback && callback();
    },

    *update({ payload, callback }, { call }) {
      let { id, sailor } = payload;
      let result = yield call(updateSailor, id, sailor);
      callback && callback()
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },

    reload(state, action) {
      return {
        ...state,
        sailor: action.payload
      }
    },

    delete(state, action) {

      let { list, pagination } = state.data;

      return {
        data: {
          list: list.filter(item => item.key !== action.payload),
          pagination: {
            total: pagination.total - 1,
            pageSize: pagination.pageSize,
            current: pagination.current,
            sorter: pagination.sorter
          }
        }
      }

    }
  },
};
