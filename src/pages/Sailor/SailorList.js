import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {routerRedux} from "dva/router";
import {
  Row,
  Col,
  Card,
  Form,
  Popconfirm,
  Input,
  Select,
  Radio,
  Button,
  Divider,
  message
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './SailorList.less';

const FormItem = Form.Item;
const { TextArea } = Input;
const { Option } = Select;

const RadioGroup = Radio.Group;


@connect(({ sailor, loading }) => ({
  sailor,
  loading: loading.models.sailor,
}))
@Form.create()
class SailorList extends PureComponent {

  state = {
    modalVisible: false,
    updateModalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
  };

  columns = [
    {
      title: '姓名',
      dataIndex: 'name',
    },
    {
      title: '常任职位',
      dataIndex: 'position',
    },
    {
      title: '手机号码',
      dataIndex: 'mobile',
    },
    {
      title: '地址',
      dataIndex: 'address'
    },
    {
      title: '是否高级船员',
      dataIndex: 'isAdvanced',
      render: val => val ? "是" : "否",
    },

    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateSailor(record)}>修改</a>
          <Divider type="vertical" />
          <span>
             <Popconfirm title="是否要删除此行？" onConfirm={() => this.handleRemoveSailor(record.key)}>
                <a>删除</a>
             </Popconfirm>
          </span>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'sailor/fetch',
    });
  }

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 4, lg: 12, xl: 24 }}>
          <Col md={6} sm={24}>
            <FormItem label="姓名">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="职位">
              {getFieldDecorator('position')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="是否高级">
              {getFieldDecorator('isAdvanced')(
                <RadioGroup>
                  <Radio value={1}>是</Radio>
                  <Radio value={0}>否</Radio>
                </RadioGroup>
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
              {/*<a style={{ marginLeft: 8 }} onClick={this.toggleForm}>*/}
              {/*展开 <Icon type="down" />*/}
              {/*</a>*/}
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  handleUpdateSailor(record) {
    this.props.dispatch(routerRedux.push(`/sailor/update/${record.key}`))
  }

  handleRemoveSailor(key) {
    this.props.dispatch({
      type: "sailor/remove",
      payload: key,
      callback: this.handleSailorRemoved
    })
  }

  handleSailorRemoved = () => {
    message.success("船员已成功删除")
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'sailor/fetch',
        payload: values,
      });
    });
  };

  handleCreateSailor = () => {
    this.props.dispatch(routerRedux.push("/sailor/add"))
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      page: pagination.current - 1,
      size: pagination.pageSize,
      ...formValues,
      ...filters,
    };

    if (sorter.field) {
      params.sort = `${sorter.field},${sorter.order === "ascend" ? "asc" : "desc"}`;
    }

    dispatch({
      type: 'sailor/fetch',
      payload: params,
    });
  };

  render() {

    const {
      sailor: { data },
      loading,
    } = this.props;

    const { selectedRows } = this.state;

    return (
      <PageHeaderWrapper title="船员列表">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={() => this.handleCreateSailor()}>
              新建
            </Button>
          </div>
          <StandardTable
            selectedRows={selectedRows}
            loading={loading}
            data={data}
            columns={this.columns}
            onSelectRow={this.handleSelectRows}
            onChange={this.handleStandardTableChange}
          />
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default SailorList;
