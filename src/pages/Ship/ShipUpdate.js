import React, { PureComponent } from 'react';
import {
  Card,
  Button,
  Form,
  Icon,
  Col,
  Row,
  DatePicker,
  Input,
  Select,
  Popover,
} from 'antd';
import { connect } from 'dva';
import FooterToolbar from '@/components/FooterToolbar';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ShipMemberForm from './ShipMemberForm';
import ShipPayloadForm from './ShipPayloadForm';
import styles from './shipAdd.less';
import {message} from "antd/lib/index";
import {routerRedux} from "dva/router";
import moment from "moment"

const { Option } = Select;

const fieldLabels = {
  name: '船舶名',
  identifier: "船舶识别号",
  owner: "船舶所有人",
  shareRemark: "船舶共有情况",
  harbor: "船籍港",
  formerName: "曾用名",
  registerNumber: "初次登记号",
  examineNumber: "船检登记号",
  material: "船舶材质",
  buildAt: "建造完工日期",
  assembleAt:"安放龙骨日期",
  type: '船舶类型',
  sailorCount: "船员总人数",
  power: '发动机功率',
  grossTone: '总吨位 (吨)',
  netTone: '净吨位 (吨)',
  length: '船身长 (米)',
  width: '船身宽 (米)',
  depth: '船身深 (米)',
  height: '船身高 (米)'
};

const types = [
  "自卸砂船",
  "集装箱船",
  "散装运泥船",
  "平板货船",
  "高速客船"
];

const materials = [
  "钢质",
  "玻璃纤维",
  "铝合金"
];

const payloadData = [];
const sailorData = [];

@connect(({ loading, ship }) => ({
  ship: ship.ship,
  submitting: loading.effects['ship/create'],
}))
@Form.create()
class UpdateShipForm extends PureComponent {

  state = {
    width: '100%',
    sailors: []
  };

  componentDidMount() {
    window.addEventListener('resize', this.resizeFooterToolbar, { passive: true });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeFooterToolbar);
  }

  resizeFooterToolbar = () => {
    requestAnimationFrame(() => {
      const sider = document.querySelectorAll('.ant-layout-sider')[0];
      if (sider) {
        const width = `calc(100% - ${sider.style.width})`;
        const { width: stateWidth } = this.state;
        if (stateWidth !== width) {
          this.setState({ width });
        }
      }
    });
  };

  setShipInfo = () => {
    const { ship, form } = this.props;

    Object.keys(form.getFieldsValue()).forEach(key => {
      const obj = {};

      if (key === "assembleAt" || key === "buildAt") {
        obj[key] = ship[key] ? moment(ship[key]) : null;
      } else {
        obj[key] = ship[key] || null;
      }

      form.setFieldsValue(obj);
    });
  };

  handleShipUpdated = () => {
    message.success('船舶信息已更新');
    this.props.dispatch(routerRedux.push('/ship/list'));
  };

  componentWillMount() {
    if (this.props.match.params.id) {
      let shipId = parseInt(this.props.match.params.id, 10);
      setTimeout(() => {
        this.props.dispatch({
          type: "ship/load",
          payload: shipId,
          callback: this.setShipInfo
        })
      }, 10)
    }
  }

  render() {
    const { form, submitting } = this.props;
    const { getFieldDecorator, validateFieldsAndScroll, getFieldsError } = form;

    const errors = getFieldsError();
    const getErrorInfo = () => {
      const errorCount = Object.keys(errors).filter(key => errors[key]).length;
      if (!errors || errorCount === 0) {
        return null;
      }
      const scrollToField = fieldKey => {
        const labelNode = document.querySelector(`label[for="${fieldKey}"]`);
        if (labelNode) {
          labelNode.scrollIntoView(true);
        }
      };
      const errorList = Object.keys(errors).map(key => {
        if (!errors[key]) {
          return null;
        }
        return (
          <li key={key} className={styles.errorListItem} onClick={() => scrollToField(key)}>
            <Icon type="cross-circle-o" className={styles.errorIcon} />
            <div>{errors[key][0]}</div>
            <div className={styles.errorField}>{fieldLabels[key]}</div>
          </li>
        );
      });
      return (
        <span className={styles.errorIcon}>
          <Popover
            title="表单校验信息"
            content={errorList}
            overlayClassName={styles.errorPopover}
            trigger="click"
            getPopupContainer={trigger => trigger.parentNode}
          >
            <Icon type="exclamation-circle" />
          </Popover>
          {errorCount}
        </span>
      );
    };

    const validate = () => {
      const { dispatch, ship } = this.props;

      validateFieldsAndScroll((error, values) => {
        if (!error) {
          // submit the values
          dispatch({
            type: 'ship/update',
            payload: {
              id: ship.id,
              ship: values
            },
            callback: this.handleShipUpdated
          });
        }
      });
    };

    const { width } = this.state;
    return (
      <PageHeaderWrapper
        title="新船舶信息"
        content="按表单提示填入相应船舶信息"
        wrapperClassName={styles.advancedForm}
      >
        <Form layout="vertical" hideRequiredMark>
          <Card title="基本信息" className={styles.card} bordered={false}>
            <Row gutter={16}>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.name}>
                  {getFieldDecorator('name', {
                    rules: [{ required: true, message: '请输入船舶名称' }],
                  })(<Input placeholder="请输入船舶名称" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.identifier}>
                  {getFieldDecorator('identifier', {
                    rules: [{ required: true, message: '请输入船舶识别号' }],
                  })(<Input placeholder="请输入船舶识别号" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.owner}>
                  {getFieldDecorator('owner', {
                    rules: [{ required: true, message: '请输入船舶所有人' }],
                  })(<Input placeholder="请输入船舶名称" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.type}>
                  {getFieldDecorator('type', {
                    rules: [{ required: true, message: '请选择船舶类型' }],
                  })(
                    <Select placeholder="请选择船舶类型">
                      {types.map((val, index) => {
                        return <Option value={index}>{val}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={16}>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.shareRemark}>
                  {getFieldDecorator('shareRemark', {
                    rules: [{ required: true, message: '请输入船舶共有情况' }],
                  })(<Input placeholder="请输入船舶共有情况" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.registerNumber}>
                  {getFieldDecorator('registerNumber', {
                    rules: [{ required: true, message: '请输入船舶初次登记号' }],
                  })(<Input placeholder="请输入船舶初次登记号" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.examineNumber}>
                  {getFieldDecorator('examineNumber', {
                    rules: [{ required: true, message: '请输入船舶船检登记号' }],
                  })(<Input placeholder="请输入船舶船检登记号" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.material}>
                  {getFieldDecorator('material', {
                    rules: [{ required: true, message: '请选择船舶材质' }],
                  })(
                    <Select placeholder="请选择船舶船舶材质">
                      {materials.map((val, index) => {
                        return <Option value={index}>{val}</Option>
                      })}
                    </Select>
                  )}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.harbor}>
                  {getFieldDecorator('harbor', {
                    rules: [{ required: true, message: '请输入船籍港' }],
                  })(<Input placeholder="请输入船籍港" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.formerName}>
                  {getFieldDecorator('formerName', {
                    rules: [{ required: true, message: '请输入曾用名' }],
                  })(<Input placeholder="请输入曾用名" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.buildAt}>
                  {getFieldDecorator('buildAt', {
                    rules: [{ required: true, type: 'object', message: '请输入建造完工日期' }],
                  })(<DatePicker  format="YYYY-MM-DD" placeholder="请输入建造完工日期" style={{ width: '100%' }}  />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.assembleAt}>
                  {getFieldDecorator('assembleAt', {
                    rules: [{ required: true, type: 'object', message: '请输入安放龙骨日期' }],
                  })(<DatePicker format="YYYY-MM-DD" placeholder="请输入安放龙骨日期" style={{ width: '100%' }}  />)}
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Card title="船舶参数" className={styles.card} bordered={false}>
            <Row gutter={16}>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.grossTone}>
                  {getFieldDecorator('grossTone', {
                    rules: [{ required: true, message: '请输入船舶总吨' }],
                  })(<Input placeholder="请输入船舶总吨" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.netTone}>
                  {getFieldDecorator('netTone', {
                    rules: [{ required: true, message: '请输入船舶净吨' }],
                  })(<Input placeholder="请输入船舶净吨" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.sailorCount}>
                  {getFieldDecorator('sailorCount', {
                    rules: [{ required: true, message: '请输入船员总人数' }],
                  })(<Input placeholder="请输入船舶船员总人数" />)}
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={16}>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.length}>
                  {getFieldDecorator('length', {
                    rules: [{ required: true, message: '请输入船舶总长' }],
                  })(<Input placeholder="请输入船舶总长" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.width}>
                  {getFieldDecorator('width', {
                    rules: [{ required: true, message: '请输入船舶总宽' }],
                  })(<Input placeholder="请输入船舶总宽" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.depth}>
                  {getFieldDecorator('depth', {
                    rules: [{ required: true, message: '请输入船舶型深' }],
                  })(<Input placeholder="请输入船舶型深" />)}
                </Form.Item>
              </Col>
              <Col lg={6} md={12} sm={24}>
                <Form.Item label={fieldLabels.height}>
                  {getFieldDecorator('height', {
                    rules: [{ required: true, message: '请输入船舶船高' }],
                  })(<Input placeholder="请输入船舶船高" />)}
                </Form.Item>
              </Col>
            </Row>
          </Card>

          <Card title="载重吨列表" className={styles.card} bordered={false}>
            {getFieldDecorator('payloads', {
              initialValue: payloadData,
            })(<ShipPayloadForm />)}
          </Card>

          <Card title="相关证书信息" className={styles.card} bordered={false}>
            <div>todo</div>
          </Card>

          <Card title="船员信息" className={styles.card} bordered={false}>
            {getFieldDecorator('sailors', {
              initialValue: sailorData,
            })(<ShipMemberForm />)}
          </Card>

          <FooterToolbar style={{ width }}>
            {getErrorInfo()}
            <Button type="primary" onClick={validate} loading={submitting}>
              提交
            </Button>
          </FooterToolbar>
        </Form>
      </PageHeaderWrapper>
    )
  }
}

export default UpdateShipForm;
