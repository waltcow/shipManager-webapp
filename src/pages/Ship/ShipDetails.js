import React, { Component } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Card, Badge, Table, Divider } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import styles from './ShipDetails.less';
import {Form} from "antd/lib/index";

const { Description } = DescriptionList;

const fieldLabels = {
  name: '船舶名',
  identifier: "船舶识别号",
  owner: "船舶所有人",
  shareRemark: "船舶共有情况",
  harbor: "船籍港",
  formerName: "曾用名",
  registerNumber: "初次登记号",
  examineNumber: "船检登记号",
  material: "船舶材质",
  buildAt: "建造完工日期",
  assembleAt:"安放龙骨日期",
  type: '船舶类型',
  sailorCount: "船员总人数",
  power: '发动机功率',
  grossTone: '总吨位 (吨)',
  netTone: '净吨位 (吨)',
  length: '船身长 (米)',
  width: '船身宽 (米)',
  depth: '船身深 (米)',
  height: '船身高 (米)'
};


const types = [
  "自卸砂船",
  "集装箱船",
  "散装运泥船",
  "平板货船",
  "高速客船"
];

const materials = [
  "钢质",
  "玻璃纤维",
  "铝合金"
];

const sailorColumn = [
  {
    title: '船员姓名',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: '职位',
    dataIndex: 'position',
    key: 'position',
  },
  {
    title: '联系电话',
    dataIndex: 'mobile',
    key: 'mobile'
  },
  {
    title: '是否高级船员',
    dataIndex: 'isAdvanced',
    key: 'isAdvanced',
    render: isAdvanced =>
      isAdvanced ? (
        <Badge status="success" text="是" />
      ) : (
        <Badge status="error" text="否" />
      ),
  },
];

const areaList = [
  "A级",
  "B级",
  "C级",
  "沿海Ⅰ类",
  "沿海ⅠⅠ类",
  "沿海ⅠⅠⅠ类"
];

const payloadColumn = [
  {
    title: '航区类型',
    dataIndex: 'area',
    key: 'area',
    render: type => `${areaList[type]}`
  },
  {
    title: '载重吨数',
    dataIndex: 'payload',
    key: 'payload',
    render: payload => `${payload} 吨`
  }
];

@connect(({ loading, ship }) => ({
  ship: ship.ship,
  loading: loading.effects['ship/load'],
}))
class ShipDetails extends Component {

  componentWillMount() {
    if (this.props.match.params.id) {
      let shipId = parseInt(this.props.match.params.id, 10);
      setTimeout(() => {
        this.props.dispatch({
          type: "ship/load",
          payload: shipId
        })
      }, 10)
    }
  }

  render() {
    let ship = this.props.ship;

    return (
      <PageHeaderWrapper title="船舶详情页">
        <Card style={{ marginBottom: 24 }} bordered={false}>
          <DescriptionList size="large" col={3} title="基本信息" style={{ marginBottom: 32 }}>
            <Description term={fieldLabels.name}>{ship.name}</Description>
            <Description term={fieldLabels.identifier}>{ship.identifier}</Description>
            <Description term={fieldLabels.owner}>{ship.owner}</Description>
            <Description term={fieldLabels.type}>{types[ship.type]}</Description>

            <Description term={fieldLabels.shareRemark}>{ship.shareRemark}</Description>
            <Description term={fieldLabels.registerNumber}>{ship.registerNumber}</Description>
            <Description term={fieldLabels.examineNumber}>{ship.owner}</Description>
            <Description term={fieldLabels.material}>{materials[ship.material]}</Description>

            <Description term={fieldLabels.harbor}>{ship.harbor}</Description>
            <Description term={fieldLabels.formerName}>{ship.formerName}</Description>
            <Description term={fieldLabels.buildAt}>{ship.buildAt ? moment(ship.buildAt).format("YYYY-MM-DD") : ""}</Description>
            <Description term={fieldLabels.assembleAt}>{ship.assembleAt ? moment(ship.assembleAt).format("YYYY-MM-DD") : ""}</Description>
          </DescriptionList>
        </Card>

        <Card style={{ marginBottom: 24 }} bordered={false}>
          <DescriptionList size="large" title="船舶参数" style={{ marginBottom: 32 }}>
            <Description term={fieldLabels.grossTone}>{ship.grossTone}</Description>
            <Description term={fieldLabels.netTone}>{ship.netTone}</Description>
            <Description term={fieldLabels.sailorCount}>{ship.sailorCount}</Description>
            <Description term={fieldLabels.length}>{ship.length}</Description>
            <Description term={fieldLabels.width}>{ship.width}</Description>
            <Description term={fieldLabels.height}>{ship.height}</Description>
            <Description term={fieldLabels.depth}>{ship.depth}</Description>
          </DescriptionList>
        </Card>

        <Card  style={{ marginBottom: 24 }} bordered={false}>
          <div className={styles.title}>船员信息</div>
          <Table pagination={false} dataSource={ship.sailors} columns={sailorColumn}/>
        </Card>

        <Card  style={{ marginBottom: 24 }} bordered={false}>
          <div className={styles.title}>载重吨信息</div>
          <Table pagination={false} dataSource={ship.payloads} columns={payloadColumn}/>
        </Card>

      </PageHeaderWrapper>
    );
  }
}

export default ShipDetails;
