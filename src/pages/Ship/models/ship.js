import { queryShipList, createShip, deleteShip, updateShip, infoShip } from '@/services/ShipAPI';

export default {
  namespace: 'ship',

  state: {
    ship: {},
    data: {
      list: [],
      pagination: {},
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      let result = yield call(queryShipList, payload);
      if (result === undefined) {
        return false
      }
      const { code, data } = result;
      if (code === "S_OK") {
        data.list = data.list.map(item => {
          item.key = item.id;
          return item;
        });
        data.pagination.current = data.pagination.current + 1;

        yield put({
          type: 'save',
          payload: data,
        });
      }
    },

    *load({ payload, callback }, { put, call }) {
      const { code, data } = yield call(infoShip, payload);

      if (code === "S_OK") {
        yield put({
          type: 'reload',
          payload: data,
        });
        callback && callback()
      }
    },


    *create({ payload, callback }, { call }) {
      let result = yield call(createShip, payload);
      callback && callback()
    },
    *remove({ payload, callback }, { call, put }) {
      let result = yield call(deleteShip, payload);

      yield put({
        type: 'delete',
        payload: payload,
      });

      callback && callback()

    },
    *update({ payload, callback }, { call, put }) {
      let { id, ship } = payload;
      let result = yield call(updateShip, id, ship);
      callback && callback()
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },

    reload(state, action) {
      return {
        ...state,
        ship: action.payload
      }
    },

    delete(state, action) {

      let { list, pagination } = state.data;

      return {
        data: {
          list: list.filter(item => item.key !== action.payload),
          pagination: {
            total: pagination.total - 1,
            pageSize: pagination.pageSize,
            current: pagination.current,
            sorter: pagination.sorter
          }
        }
      }
    }
  },
};
