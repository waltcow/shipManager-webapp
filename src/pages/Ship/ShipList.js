import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import {routerRedux} from "dva/router";
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Select,
  Button,
  Divider,
  message,
  Popconfirm
} from 'antd';
import StandardTable from '@/components/StandardTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './ShipList.less';

const FormItem = Form.Item;

const { Option } = Select;

@connect(({ ship, loading }) => ({
  ship,
  loading: loading.models.ship,
}))
@Form.create()
class ShipList extends PureComponent {
  state = {
    modalVisible: false,
    updateModalVisible: false,
    selectedRows: [],
    formValues: {},
    stepFormValues: {},
  };

  types = [
    "自卸砂船",
    "集装箱船",
    "散装运泥船",
    "平板货船",
    "高速客船"
  ];

  columns = [
    {
      title: '船舶名',
      dataIndex: 'name',
    },
    {
      title: '类型',
      dataIndex: 'type',
      render: val => `${this.types[val]}`,
    },
    {
      title: '船舶共有情况',
      dataIndex: 'shareRemark'
    },
    {
      title: '总吨数',
      dataIndex: 'grossTone',
      sorter: true,
      render: val => `${val} 吨`,
    },
    {
      title: '净吨数',
      dataIndex: 'netTone',
      sorter: true,
      render: val => `${val} 吨`,
    },

    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleUpdateShip(record)}>修改</a>
          <Divider type="vertical" />
          <a onClick={() => this.handleViewShip(record)}>详情</a>
          <Divider type="vertical" />
          <Popconfirm title="是否要删除此行？" onConfirm={() => this.handleDeleteShip(record.key)}>
            <a>删除</a>
          </Popconfirm>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'ship/fetch',
    });
  }

  handleStandardTableChange = (pagination, filtersArg, sorter) => {
    const { dispatch } = this.props;
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      page: pagination.current - 1,
      size: pagination.pageSize,
      ...formValues,
      ...filters,
    };

    if (sorter.field) {
      params.sort = `${sorter.field},${sorter.order === "ascend" ? "asc" : "desc"}`;
    }

    dispatch({
      type: 'ship/fetch',
      payload: params,
    });
  };

  handleDeleteShipSuccess = () => {
    message.success("船舶删除成功")
  };

  handleUpdateShip = (ship) => {
    this.props.dispatch(routerRedux.push(`/ship/update/${ship.id}`));
  };

  handleViewShip = (ship) => {
    this.props.dispatch(routerRedux.push(`/ship/details/${ship.id}`));
  };

  handleDeleteShip = (key) => {
    const { dispatch } = this.props;
    dispatch({
      type: "ship/remove",
      payload: key,
      callback: this.handleDeleteShipSuccess
    })
  };

  handleCreateShip = () => {
    this.props.dispatch(routerRedux.push("/ship/add"))
  };

  handleFormReset = () => {
    const { form, dispatch } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });
    dispatch({
      type: 'ship/fetch',
      payload: {},
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue
      };

      this.setState({
        formValues: values,
      });

      dispatch({
        type: 'ship/fetch',
        payload: values,
      });
    });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  renderSimpleForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="船舶名称">
              {getFieldDecorator('name')(<Input placeholder="请输入" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="船舶类型">
              {getFieldDecorator('type')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {this.types.map((val, index) => {
                    return <Option value={index}>{val}</Option>
                  })}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                查询
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                重置
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  render() {
    const {
      ship: { data },
      loading,
    } = this.props;

    const { selectedRows } = this.state;

    return (
      <PageHeaderWrapper title="船舶列表">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderSimpleForm()}</div>
          </div>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={this.handleCreateShip}>
              新建
            </Button>
          </div>
          <StandardTable
            selectedRows={selectedRows}
            loading={loading}
            data={data}
            columns={this.columns}
            onSelectRow={this.handleSelectRows}
            onChange={this.handleStandardTableChange}
          />
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default ShipList;
