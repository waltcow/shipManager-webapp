import request from "@/utils/request";
import { stringify } from 'qs';

export async function querySailorList(params) {
  return request(`/api/v1/sailor/list?${stringify(params)}`)
}

export async function querySailorDetails(params) {
  return request(`/api/v1/sailor/${params}`)
}

export async function deleteSailor(params) {
  return request(`/api/v1/sailor/${params}`, {
    method: 'DELETE',
  })
}

export async function updateSailor(id, params) {
  return request(`/api/v1/sailor/${id}`, {
    method: 'PUT',
    body: params
  })
}

export async function createSailor(params) {
  return request('/api/v1/sailor/', {
    method: 'POST',
    body: params
  });
}
