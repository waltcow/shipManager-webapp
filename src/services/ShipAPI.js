import request from "@/utils/request";
import { stringify } from 'qs';

export async function queryShipList(params) {
  return request(`/api/v1/ship/list?${stringify(params)}`)
}

export async function createShip(params) {
  return request('/api/v1/ship/', {
    method: 'POST',
    body: params
  });
}

export async function infoShip(id) {
  return request(`/api/v1/ship/${id}`)
}

export async function deleteShip(id) {
  return request(`/api/v1/ship/${id}`, {
    method: 'DELETE'
  });
}

export async function updateShip(id, params) {
  return request(`/api/v1/ship/${id}`, {
    method: 'PUT',
    body: params
  })
}
