import request from '@/utils/request';

export async function query() {
  return request('/api/users');
}

export async function queryCurrent() {
  return request('/api/v1/user/info');
}

export async function updateCurrent(id, params) {
  return request(`/api/v1/user/update/${id}`, {
    method: "POST",
    body: params
  });
}
