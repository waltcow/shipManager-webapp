import request from "@/utils/request";

export async function accountLogin(params) {
  return request('/api/v1/global/login', {
    method: 'POST',
    body: params,
  });
}

export async function getSTSToken() {
  return request("/api/v1/third_party/oss/token")
}
