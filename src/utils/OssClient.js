import OSS from 'oss'
import { getSTSToken } from '@/services/AuthAPI';

let _instance = null;

export default class OssClient {

  static getInstance() {
    if (_instance === null) {
      _instance = new OssClient()
    }
    return _instance
  }

  constructor() {
    this.fetchToken();
  }

  fetchToken() {
    getSTSToken().then(response => {
      if (response.code === "S_OK") {
        const {
          accessKeyId,
          accessKeySecret,
          securityToken,
          expiration
        } = response.data;

        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
        this.securityToken = securityToken;
        this.expiration = expiration;

        this._client = new OSS.Wrapper({
          accessKeyId: accessKeyId,
          accessKeySecret: accessKeySecret,
          stsToken: securityToken,
          region: OSS_ENDPOINT,
          bucket: OSS_BUCKET,
        });

      }
    });
  }

  updateToOss(file, remotePath = "/data") {
    let client = this._client;
    if (!client) {
      return Promise.reject("oss client initialize failed")
    }

    let uploadPath = `${remotePath}/${file.name.split(".")[0]}_${new Date().getTime()}.${file.type.split("/")[1]}`;

    return new Promise((resolve, reject) => {
      client.multipartUpload(uploadPath, file).then(data => {
        resolve(data);
      }).catch(error => {
        reject(error)
      })
    })
  }
}
