// use localStorage to store the authority info, which might be sent from server in actual project.
export function getAuthority() {
  // return localStorage.getItem('antd-pro-authority') || ['admin', 'user'];
  let authority = localStorage.getItem('ship-manager-authority');

  if (authority) {
    if (authority.includes('[')) {
      authority = JSON.parse(authority);
    } else {
      authority = [authority];
    }
  }
  return authority;
}

export function setAuthority(authority) {
  if (authority === 0) {
     localStorage.setItem('ship-manager-authority', JSON.stringify(['guest']));
  } else if (authority === 1) {
    localStorage.setItem('ship-manager-authority', JSON.stringify(['admin']));
  } else {
    localStorage.removeItem("ship-manager-authority")
  }
}

export function updateToken(token) {
  if (token === null) {
    localStorage.removeItem("ship-manager-token")
  } else {
    localStorage.setItem('ship-manager-token', token);
  }
}

export function getToken() {
  return localStorage.getItem("ship-manager-token")
}
